﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //Enumerators
    enum PLAYER_ID { PLAYER_1, PLAYER_2 };
    enum PLAYER_DIRECTION { UP, RIGHT, LEFT };


    //Public
    public int mSpeed;
    public int mJumpForce;
    public bool isGrounded;
    public bool isAttacking;
    public bool isDead;


    //Serialized
    [SerializeField] PLAYER_ID mPlayerID;


    //Private
    private PLAYER_DIRECTION mDirection;
    private SpriteRenderer mSpriteRenderer;
    private Rigidbody2D mRigidbody;
    private Animator animator;

    void Start()
    {
        mSpriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        mRigidbody = GetComponent<Rigidbody2D>();
        isGrounded = true;
    }


    void Update()
    {
        playerMovement();
    }

    void playerMovement()
    {

        switch (mPlayerID)
        {
            case PLAYER_ID.PLAYER_1:

                if (Input.GetKeyDown(KeyCode.F))
                {
                    if (isGrounded )
                    {
                        meleeAttackAnimation();
                     
                    }
                       

                }
                else if (Input.GetKey(KeyCode.A) && !isAttacking)
                {
                    //Player 1 Left
                    this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x - mSpeed * Time.deltaTime, this.gameObject.transform.position.y);
                    mDirection = PLAYER_DIRECTION.LEFT;
                    flipSprite(mDirection);
                    if (isGrounded)
                    {
                        runAnimation();
                    }
                }
                else if (Input.GetKey(KeyCode.D) && !isAttacking)
                {
                    //Player 1 Right
                    this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + mSpeed * Time.deltaTime, this.gameObject.transform.position.y);
                    mDirection = PLAYER_DIRECTION.RIGHT;
                    flipSprite(mDirection);
                    if (isGrounded)
                    {
                        runAnimation();
                    }

                }
                else
                {
                    if (isGrounded)
                    {
                        //Player 1 Idle
                        idleAnimation();
                    }
                }
                if (isGrounded)
                {
                    if (Input.GetKeyDown(KeyCode.W) && !isAttacking)
                    {
                        //Player 1 Up
                        jumpAnimation();
                        mRigidbody.AddForce(new Vector2(0, mJumpForce));
                        isGrounded = false;

                    }
                }


                break;

            case PLAYER_ID.PLAYER_2:


                if (Input.GetKeyDown(KeyCode.RightShift))
                {
                    if (isGrounded )
                    {
                        meleeAttackAnimation();
                     
                    }
                       

                }else if (Input.GetKey(KeyCode.LeftArrow) && !isAttacking)
                {
                    //Player 1 Left
                    this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x - mSpeed * Time.deltaTime, this.gameObject.transform.position.y);
                    mDirection = PLAYER_DIRECTION.LEFT;
                    flipSprite(mDirection);
                    if (isGrounded)
                    {
                        runAnimation();
                    }
                }
                else if (Input.GetKey(KeyCode.RightArrow) && !isAttacking)
                {
                    //Player 1 Right
                    this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x + mSpeed * Time.deltaTime, this.gameObject.transform.position.y);
                    mDirection = PLAYER_DIRECTION.RIGHT;
                    flipSprite(mDirection);
                    if (isGrounded)
                    {
                        runAnimation();
                    }

                }
                else
                {
                    if (isGrounded)
                    {
                        //Player 1 Idle
                        idleAnimation();
                    }
                }
                if (isGrounded)
                {
                    if (Input.GetKeyDown(KeyCode.UpArrow) && !isAttacking)
                    {
                        //Player 1 Up
                        jumpAnimation();
                        mRigidbody.AddForce(new Vector2(0, mJumpForce));
                        isGrounded = false;

                    }
                }

                break;
        }
    }

    void flipSprite(PLAYER_DIRECTION direction)
    {
        switch (direction)
        {
            case PLAYER_DIRECTION.LEFT:
                if (transform.localScale.x > 0)
                    transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);

                break;

            case PLAYER_DIRECTION.RIGHT:
                if (transform.localScale.x < 0)
                    transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
                break;

        }
    }


    //ANIMATIONS
    void runAnimation()
    {
        animator.SetBool("isRunning", true);
        animator.SetBool("isStop", false);
        animator.SetBool("isJumping", false);
    }

    void idleAnimation()
    {
        animator.SetBool("isRunning", false);
        animator.SetBool("isJumping", false);
        animator.SetBool("isStop", true);
    }

    void jumpAnimation()
    {
        animator.SetBool("isJumping", true);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStop", false);
    }

    void meleeAttackAnimation()
    {
        animator.SetBool("isJumping", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStop", false);
        animator.SetBool("isAtacking", true);
        isAttacking = true;
    }

    void deadAnimation()
    {
        animator.SetBool("isJumping", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStop", false);
        animator.SetBool("isAtacking", false);
        animator.SetBool("isDead", true);

    }

    public void enableMeleeAtack()
    {
        this.transform.GetChild(0).gameObject.SetActive(true);
        //this.gameObject.GetComponents<Collider2D>()[1].enabled = true;

    }

    public void disableMeleeAttack()
    {
        this.transform.GetChild(0).gameObject.SetActive(false);
        animator.SetBool("isAtacking", false);
        isAttacking = false;

    }

    public void resetAnimations()
    {
         animator.SetBool("isJumping", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStop", true);
        animator.SetBool("isAtacking", false);
        animator.SetBool("isDead", false);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {

     switch(mPlayerID)
     {
         case PLAYER_ID.PLAYER_1:
            if(other.tag == "Player2Attack")
            {
                Debug.Log("Ola");
                isDead = true;
                deadAnimation();
            }

         break;

        case PLAYER_ID.PLAYER_2:

            if(other.tag == "Player1Attack")
            {
                                Debug.Log("Ola");

                isDead = true;
                deadAnimation();

            }


        break;


     }

    }


}
